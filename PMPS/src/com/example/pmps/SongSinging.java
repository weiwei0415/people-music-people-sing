package com.example.pmps;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class SongSinging extends MySongActivity{
	//長按menu選項宣告
	public static final int playID = Menu.FIRST+4;
	public static final int recordID = Menu.FIRST+5;	
	//判斷播放和K歌選擇
	public static final int isPlay = 1;
	public static final int isRecord = 2;
	
	//建立Menu選項的事件
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		menu.setHeaderTitle("Menu");
		currentPosition = ((AdapterContextMenuInfo)menuInfo).position;
		songPath = path.get(currentPosition);
		
		menu.clear();
		
		menu.add(0,playID,0,"播放模式");
		menu.add(0,recordID,0,"歡唱模式(錄音)");
		menu.add(0,exitID,0,"返回");
	}
    
    //Menu選項被選時執行的事件
    public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		super.onContextItemSelected(item);
		switch( item.getItemId() )
		{
		    case playID:
	    	    Intent intent = new Intent(); //建立一個intent
	    	    intent.setClass(SongSinging.this, Player.class);
	    	
	    	    Bundle bundle = new Bundle();
	    	    bundle.putString("mTitle",title.get(currentPosition));
	    	    bundle.putString("mArtist",artist.get(currentPosition));
    	        bundle.putString("mPath",path.get(currentPosition));
    	        bundle.putInt("isChoose",isPlay);
    	        
    	        intent.putExtras(bundle);
    	        startActivity(intent);
		        break;    
		    case recordID:    
		    	Intent intent2 = new Intent(); //建立一個intent
	    	    intent2.setClass(SongSinging.this, Player.class);
	    	
	    	    Bundle bundle2 = new Bundle();
	    	    bundle2.putString("mTitle",title.get(currentPosition));
	    	    bundle2.putString("mArtist",artist.get(currentPosition));
    	        bundle2.putString("mPath",path.get(currentPosition));
    	        bundle2.putInt("isChoose",isRecord);
    	    
    	        intent2.putExtras(bundle2);
    	        startActivity(intent2);
				break;	
		    case exitID:
			    break;
		}
		return true;
	}	
}
