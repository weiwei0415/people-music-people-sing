package com.example.pmps;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.media.AudioManager;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.PresetReverb;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

public class EqualizerActivity extends Activity{
	private Equalizer myEqualizer;
	private BassBoost myBassBoost;
	private PresetReverb myPresetReverb;
	private LinearLayout myLayout;
	private List<Short> li1=new ArrayList<Short>();
	private List<String> li2=new ArrayList<String>();
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.equalizer_page);   
		setVolumeControlStream(AudioManager.STREAM_MUSIC); 
		//Layout設定
		myLayout = new LinearLayout(this);
		myLayout.setOrientation(LinearLayout.VERTICAL);
		myLayout.setBackgroundResource(R.drawable.background4);
		setContentView(myLayout);
		
		setupEqualizer();  //等化器介面設定	    
		setupBassBoost();  //重低音介面設定
		setupPresetReverb();  //音場介面設定   
	}
	//設定等化器介面的method
	private void setupEqualizer(){
		//取得MediaPlayer的AudioSessionId決定要控制的音樂
	    myEqualizer = new Equalizer(0,com.example.pmps.Player.mediaplayer.getAudioSessionId());
	    myEqualizer.setEnabled(true);
	
	    TextView eqTitle = new TextView(this);
	    eqTitle.setGravity(Gravity.CENTER);
	    eqTitle.setTextColor(R.color.red);
	    eqTitle.setTextSize(24);
	    eqTitle.setText("等化器");
	    myLayout.addView(eqTitle);
	   
	    //取得等化器種類並產生控制介面
	    short eqLevels = myEqualizer.getNumberOfBands();
	    final short minEQLevel = myEqualizer.getBandLevelRange()[0];
	    short maxEQLevel = myEqualizer.getBandLevelRange()[1];
	    for(short i=0;i<eqLevels;i++)
	    {
		      final short eqLevel=i;
		      TextView eqTextView=new TextView(this);
		      eqTextView.setLayoutParams(new ViewGroup.LayoutParams(
		                 ViewGroup.LayoutParams.FILL_PARENT,
		                 ViewGroup.LayoutParams.WRAP_CONTENT));
		      eqTextView.setGravity(Gravity.CENTER_HORIZONTAL);
		      eqTextView.setTextColor(R.color.red);
		      eqTextView.setText((myEqualizer.getCenterFreq(eqLevel)/1000)+" Hz");     
		      myLayout.addView(eqTextView);
		
		      LinearLayout tmpLayout = new LinearLayout(this);
		      tmpLayout.setOrientation(LinearLayout.HORIZONTAL);
		      TextView minDbTextView = new TextView(this);
		      
		      minDbTextView.setLayoutParams(new ViewGroup.LayoutParams(
		                    ViewGroup.LayoutParams.WRAP_CONTENT,
		                    ViewGroup.LayoutParams.WRAP_CONTENT));
		      minDbTextView.setTextColor(R.color.red);
		      minDbTextView.setText((minEQLevel / 100)+" dB");
		      		
		      TextView maxDbTextView = new TextView(this);
		      maxDbTextView.setLayoutParams(new ViewGroup.LayoutParams(
		                    ViewGroup.LayoutParams.WRAP_CONTENT,
		                    ViewGroup.LayoutParams.WRAP_CONTENT));
		      maxDbTextView.setTextColor(R.color.red);
		      maxDbTextView.setText((maxEQLevel / 100)+" dB");
		      
		      LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
		                    ViewGroup.LayoutParams.FILL_PARENT,
		                    ViewGroup.LayoutParams.WRAP_CONTENT);
		      layoutParams.weight = 1;
		      //使用SeekBar做為調整工具
		      SeekBar bar = new SeekBar(this);
		      bar.setLayoutParams(layoutParams);
		      bar.setMax(maxEQLevel - minEQLevel);
		      bar.setProgress(myEqualizer.getBandLevel(eqLevel));
		      //設定SeekBar的OnSeekBarChangeListener
		      bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
		      {
			        public void onProgressChanged(SeekBar seekBar,int progress,
			                                      boolean fromUser)
			        {
			          //設定BandLevel
			          myEqualizer.setBandLevel(eqLevel,(short)(progress + minEQLevel));
			        }
			        public void onStartTrackingTouch(SeekBar seekBar){}
			        public void onStopTrackingTouch(SeekBar seekBar){}
		      });
		      tmpLayout.addView(minDbTextView);
		      tmpLayout.addView(bar);
		      tmpLayout.addView(maxDbTextView);
		      myLayout.addView(tmpLayout);
	    }
	}
    //設定重低音介面的method
	private void setupBassBoost(){
		//取得MediaPlayer的AudioSessionId決定要控制的音樂
		myBassBoost = new BassBoost(0,com.example.pmps.Player.mediaplayer.getAudioSessionId());
		myBassBoost.setEnabled(true);

		TextView bbTitle = new TextView(this);
		bbTitle.setGravity(Gravity.CENTER);
		bbTitle.setTextColor(R.color.red);
		bbTitle.setTextSize(24);
		bbTitle.setText("\r\n重低音");
		myLayout.addView(bbTitle);
		
		SeekBar bar = new SeekBar(this);  //使用SeekBar做為調整工具
    
		bar.setMax(1000);  //重低音的範圍為0~1000
		bar.setProgress(0);
		//設定SeekBar的OnSeekBarChangeListener
		bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
		{
			public void onProgressChanged(SeekBar seekBar,int progress,boolean fromUser)
			{
				myBassBoost.setStrength((short)progress);  //設定重低音
			}
			public void onStartTrackingTouch(SeekBar seekBar){}
			public void onStopTrackingTouch(SeekBar seekBar){}
		});
		myLayout.addView(bar);
	}    
	//設定音場介面的method
	private void setupPresetReverb(){
		//取得MediaPlayer的AudioSessionId決定要控制的音樂
		myPresetReverb=new PresetReverb(0,com.example.pmps.Player.mediaplayer.getAudioSessionId());
		myPresetReverb.setEnabled(true);
          
		TextView prTitle = new TextView(this); 
		prTitle.setGravity(Gravity.CENTER);
		prTitle.setTextColor(R.color.red);
		prTitle.setTextSize(24);
		prTitle.setText("\r\n音場");
		myLayout.addView(prTitle);
		//取得音場種類及名稱
		for(short i=0;i<myEqualizer.getNumberOfPresets();i++)
		{
			final short pr = i;
			li1.add(pr);
			li2.add(myEqualizer.getPresetName(pr));
		}
		//使用Spinner做為音場選擇工具
		Spinner sp=new Spinner(this);
		sp.setAdapter(new ArrayAdapter<String>(EqualizerActivity.this,
                  android.R.layout.simple_spinner_dropdown_item,li2));
		//設定Spinner的OnItemSelectedListener
		sp.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2,long arg3)
			{
				myPresetReverb.setPreset(li1.get(arg2));  //設定音場
			}
			public void onNothingSelected(AdapterView<?> arg0)
			{
			}
		});
		myLayout.addView(sp);
	}
}

