package com.example.pmps;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class Player extends Activity implements OnClickListener{
	//介面宣告
	private TextView mTitle;
	private TextView mArtist;
	private SeekBar mSeekbar;
	private TextView mStart;
	private TextView mDuration;
	private ImageButton playButton;
	private ImageButton stopButton;
	private ImageButton vocalButton;
	private ImageButton eqButton;
	//判斷player狀態
	private boolean bIsPaused = false; //判斷是否為暫停狀態
	private boolean isPlaying = true;  //判斷是否為播放狀態
	private boolean isStop = false;	   //判斷是否為停止狀態
	private boolean bIsReleased = false; 
	//暫存歌曲基本資訊
	private String Title;
	private String Artist;
	private String Path;
	//暫存目前錄音基本資訊
	private String songTitle;
	private String songSize;
	private String songPath;
	private String songDate;
	//SQLite宣告
	MyDBHelper rHelper = null;
	SQLiteDatabase rDB = null;
	Cursor rCursor = null;
	// 定義錄音檔存放位置
	private static final String FOLDER_PATH="/mnt/sdcard/Music";
	// 定義臨時儲存檔
	private File audioRecFile;
		
	public static MediaPlayer mediaplayer;
	private MediaRecorder recorder; 
	public static AudioTrack audioTrack = null;
	private Handler mHandler = new Handler();
	private UpdateStatus mus; 
	private int isChoose;

	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.player_page);
	        
	    Bundle bundle = this.getIntent().getExtras();
	    Title = bundle.getString("mTitle");
	    Artist = bundle.getString("mArtist");
	    Path = bundle.getString("mPath");
	    isChoose = bundle.getInt("isChoose");
			
		mTitle = (TextView)findViewById(R.id.title);
		mArtist = (TextView)findViewById(R.id.artist);	

	    mTitle.setText(Title);
	    mArtist.setText(Artist);
	    
	    initPlay();
	    initControls();
	    
	    if(isChoose == 2){
	    	startRecording(); 
	    	playButton.setImageResource(R.drawable.av_record);
	    	playButton.setClickable(false);
	    	vocalButton.setImageResource(R.drawable.recordicon);
	    }
	    else{
	    	Toast.makeText(this, "播放模式開啟", Toast.LENGTH_LONG).show();
	    	vocalButton.setImageResource(R.drawable.playicon);
	    }
	    
	    openDatabase();
	    	
	    mus = new UpdateStatus(); //start update thread and media player
		mus.start();     
		
		//當音樂播完會執行的Listener 
		 mediaplayer.setOnCompletionListener(new OnCompletionListener() 
		 { 
			 public void onCompletion(MediaPlayer arg0) 
			 {  
				    isPlaying = false;
				    isStop = true;
				    mediaplayer.reset();
				    playButton.setImageResource(R.drawable.av_play);
				    playButton.setClickable(true);
				    mSeekbar.setProgress(0);
				    mStart.setText("0:00");
				 
				 if(isChoose ==2){
				    //停止錄音，釋放錄音資源
			        if (recorder != null) 
			        {  
			            stopRecording(); 
			        }  
			        //清空錄音物件
			        recorder = null;   
			        playButton.setImageResource(R.drawable.av_record); 
				    playButton.setClickable(true);
			    }
			     else{
			    	 playButton.setImageResource(R.drawable.av_play);
			    	 playButton.setClickable(true);
			     }
				 
			 }
		  });
	}
		
	protected void onDestroy(){
		super.onDestroy();	
		//釋放播放與錄音資源  
        if (recorder != null)
        {  
            recorder.release();  
            recorder = null;  
        }  
        if (mediaplayer != null) 
        {  
        	mediaplayer.release();  
        	mediaplayer = null;  
        }  		
        closeDatabase();
	}
	
	private void openDatabase(){
		rHelper = new MyDBHelper(this); 
    }

    private void closeDatabase(){
    	rHelper.close();
    }
	
    private void initPlay(){
		if(mediaplayer == null){
	    	mediaplayer = new MediaPlayer();
		}
		else{
			mediaplayer.stop();
			mediaplayer = null;
			mediaplayer = new MediaPlayer();
	    }
	
	    try {
	    	mediaplayer.setDataSource(Path);
	    	mediaplayer.prepare();
	    } catch (IllegalArgumentException e) {
	    	e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    mediaplayer.start();
	    isPlaying = true;
	}
	
	//開始錄音
	private void startRecording() {
		// 檢查SD卡是否有效並同時處於可寫狀態
		if (CommonUtils.isSdCardAvailable() && !CommonUtils.isSdCardReadOnly()) {
			// 判斷儲存音訊的資料夾是否存在，如果不存在則進行新增操作
			File audioRecPath=new File(FOLDER_PATH);
			if (!audioRecPath.exists()) {
				audioRecPath.mkdir();
			}

			try {
				// 如果存放檔路徑正確且SD卡空間有效，則進行錄音操作
				if (audioRecPath != null
						&& CommonUtils.isSdCardStorageAvailable()) {
					
					// 新增暫存檔案，以Record_開頭
					audioRecFile=File.createTempFile("Record_", ".amr",
							audioRecPath);
					// 建構MediaRecorder對象
					recorder=new MediaRecorder();
					// 設置採樣的音訊源為麥克風
					recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
					// 設置輸出檔案格式
					recorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
					// 設置音訊的編碼方式
					recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
					// 設置輸出檔
					recorder.setOutputFile(audioRecFile.getAbsolutePath());
					// 準備錄音操作
					recorder.prepare();
					// 開始錄音
					recorder.start();
					
					Toast.makeText(this, "歡唱模式開啟", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
	}
	//停止錄音
	private void stopRecording() {
		// 如果正在錄音
		if (audioRecFile !=null) {
			// 停止錄音操作
			recorder.stop();
			// 釋放MediaRecorder對象所佔用資源
			recorder.release();
			// 將當前錄音文件添加到列表
			songTitle = audioRecFile.getName();
			songSize = CommonUtils.getFileSize(audioRecFile.length());
			songPath = audioRecFile.getAbsolutePath();
			songDate = CommonUtils.getCurrentDate();
			
			writeRecordInfo_SQL(songTitle,songSize,songPath,songDate);
			playButton.setImageResource(R.drawable.av_record);
			playButton.setClickable(true);
			Toast.makeText(this, "歡唱完畢，保存檔案成功！", Toast.LENGTH_LONG).show();
		}
	}
	//將錄音資料寫入"Record"table
	public void writeRecordInfo_SQL(String songTitle,String songSize,String songPath,String songDate){
		rDB = rHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();

    	values.put("Record_name",songTitle);
    	values.put("Record_size",songSize);
    	values.put("Record_path",songPath);
    	values.put("Record_date",songDate);
    	rDB.insert("Record",null,values);	    		

    	rDB.close();
	}
	//player介面設定
	private void initControls() {		
		playButton = (ImageButton)findViewById(R.id.play);
		playButton.setId(1);
		playButton.setOnClickListener(this);
					
		stopButton = (ImageButton) findViewById(R.id.stop);
		stopButton.setId(2);
		stopButton.setOnClickListener(this);
		
		eqButton = (ImageButton) findViewById(R.id.eq);
		eqButton.setId(3);
		eqButton.setOnClickListener(this);
		
		vocalButton = (ImageButton) findViewById(R.id.vocal);
		vocalButton.setId(4);
		vocalButton.setOnClickListener(this);
				
		mStart = (TextView)findViewById(R.id.playtime);
	    mDuration = (TextView)findViewById(R.id.duration);		
		mSeekbar = (SeekBar)findViewById(R.id.seekBar1);
		mSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

		public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
			if(fromUser)
			{
				mediaplayer.seekTo(progress);
				mSeekbar.setProgress(progress);
			}
		}	
		public void onStartTrackingTouch(SeekBar seekBar) {
				mediaplayer.pause();
		}	
		public void onStopTrackingTouch(SeekBar seekBar) {
				mediaplayer.start();
		}});			
	}
		
	public void onClick(View v) {
		switch(v.getId()){	
		case 1:	
			if(isStop==true){
				play();
			}
			else{
				pause();
			}
			break;	
		case 2:	
			stop();
			break;	
		case 3:	
			Intent intent = new Intent();  
			intent.setClass(Player.this,EqualizerActivity.class);
			startActivity(intent);
			break;			
		default :
		    break;
		}
	}
		
	private void play(){
		try 
		{ 
			if(mediaplayer.isPlaying()==true) 
			{
			    mediaplayer.reset();  //把MediaPlayer重設
			}
			playButton.setImageResource(R.drawable.av_pause);
		    
		    mediaplayer.setDataSource(Path);  //設定 MediaPlayer讀取SDcard的檔案
		    mediaplayer.prepare();	         
		    mediaplayer.start();  //把MediaPlayer開始播放
		         
		    //判斷錄音模式
			if(isChoose == 2){
			    startRecording();
			    playButton.setClickable(false);
			    playButton.setImageResource(R.drawable.av_record);
			}      
		    isPlaying = true;
		    isStop = false;
	     }
		 catch (IllegalStateException e) 
		 { 
		     e.printStackTrace();
		 }
		 catch (IOException e) 
		 { 
		     e.printStackTrace();
		 }
		 //當音樂播完會執行的Listener 
		 mediaplayer.setOnCompletionListener(new OnCompletionListener() 
		 { 
			 public void onCompletion(MediaPlayer arg0) 
			 {  
				    isPlaying = false;
				    isStop = true;
				    mediaplayer.reset();
				    playButton.setImageResource(R.drawable.av_play);
				    playButton.setClickable(true);
				    mSeekbar.setProgress(0);
				    mStart.setText("0:00");
				 
				 if(isChoose ==2){
				    //停止錄音，釋放錄音資源
			        if (recorder != null) 
			        {  
			            stopRecording(); 
			        }  
			        //清空錄音物件
			        recorder = null;   
			        playButton.setImageResource(R.drawable.av_record); 
				    playButton.setClickable(true);
			    }
			     else{
			    	 playButton.setImageResource(R.drawable.av_play);
			    	 playButton.setClickable(true);
			     }
				 
			 }
		  });	
			
		  mSeekbar.setMax(mediaplayer.getDuration());
		  mSeekbar.setProgress(mediaplayer.getCurrentPosition());
        }
		
	private void pause(){		
	    if (mediaplayer != null) { 
	        if(bIsReleased == false) { 
	            if(bIsPaused==false) { 
	                //設定 MediaPlayer暫停播放
	            	mediaplayer.pause();
	                bIsPaused = true;
	                playButton.setImageResource(R.drawable.av_play);
	                isPlaying = false;
	            }
	            else if(bIsPaused==true) 
	            { 
	                //設定 MediaPlayer播放
	            	mediaplayer.start();
	                bIsPaused = false;
	                playButton.setImageResource(R.drawable.av_pause);
	                isPlaying = true;
	            }
	        }
	    }
	}

	private void stop(){
	    if(mediaplayer.isPlaying()==true) { 
	    	//將 MediaPlayer重設
	    	isPlaying = false;
	    	isStop = true;
	    	mediaplayer.reset();
	    	playButton.setImageResource(R.drawable.av_play);
	    	playButton.setClickable(true);
	    	mSeekbar.setProgress(0);
	    	mStart.setText("0:00");			
	    }
	    
	    if(isChoose ==2){
	    	//停止錄音，釋放錄音資源
            if (recorder != null) 
            {  
                stopRecording(); 
            }  
            //清空錄音物件
            recorder = null;   
    	}
	}	

	class UpdateStatus extends Thread{   	
		public void run() {
			while (true) {
				if(isPlaying)
					mHandler.post(new Runnable() {
						public void run(){ 								
							try {							
								mSeekbar.setMax(mediaplayer.getDuration());
								mSeekbar.setProgress(mediaplayer.getCurrentPosition());
									
								int pos = 0;
								pos = mediaplayer.getCurrentPosition();
								int min = (pos/1000)/60;
								int sec = (pos/1000)%60;
								int maxPos = mediaplayer.getDuration();
								int maxMin = (maxPos/1000)/60;
								int maxSec = (maxPos/1000)%60;
								String maxTime = new String();
									
								if(maxSec<10)								
									maxTime = ""+maxMin+":0"+maxSec;
								else
									maxTime = ""+maxMin+":"+maxSec; 
									
								mDuration.setText(maxTime);
									
								if(sec<10)
									mStart.setText(""+min+":0"+sec);
								else
									mStart.setText(""+min+":"+sec);
									
							} catch (Exception e) {
								e.printStackTrace();
							}									
						}
					});
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}						
	    }
	}
}
