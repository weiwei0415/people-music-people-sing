package com.example.pmps;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.widget.MediaController;
import android.widget.VideoView;
 
public class MVPlayer extends Activity {
	private String Path;
	@Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.mvplayer_page);
	  
	  Bundle bundle = this.getIntent().getExtras();
	  Path = bundle.getString("mPath");
	 
	  VideoView videoView = (VideoView) this.findViewById(R.id.videoView);
	  MediaController mc = new MediaController(this);
	  videoView.setMediaController(mc);
	   
	   
	   
	  videoView.setVideoPath(Path);
	   
	  /*其他 sdcard 路徑的範例，給大家參考  
	  videoView.setVideoURI(
	   Uri.parse("file://" +
	   Environment.getExternalStoragePublicDirectory(
	   Environment.DIRECTORY_MOVIES) + "/testmovie.mp4"));
	  */
	   
	   
	  videoView.requestFocus();
	  videoView.start();
	 
	 }
	 

	 
	}