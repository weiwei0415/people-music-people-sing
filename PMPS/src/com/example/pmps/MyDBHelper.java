package com.example.pmps;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBHelper extends SQLiteOpenHelper {
        final private static int _DB_VERSION = 1;
        final private static String _DB_DATABASE_NAME = "MyDatabases.db";
        private SQLiteDatabase db;
        
        public MyDBHelper(Context context) {
                super(context,_DB_DATABASE_NAME,null,_DB_VERSION);
                db = this.getWritableDatabase();
        }
        
        public MyDBHelper(Context context, String name, CursorFactory factory, int version) {
                super(context, name, factory, version);
                
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE Song (" +
                        "Song_ID INTEGER PRIMARY KEY, " +
                        "Song_name VARCHAR(50) , " +
                        "Song_artist VARCHAR(50) , " +
                        "Song_album VARCHAR(50), " +
                        "Song_size  VARCHAR(50), " +
                        "Song_duration  VARCHAR(50), " +
                        "Song_path  VARCHAR(50));");
            
            db.execSQL("CREATE TABLE MV (" +
                    "MV_ID INTEGER PRIMARY KEY, " +
                    "MV_name VARCHAR(50) , " +
                    "MV_artist VARCHAR(50) , " +
                    "MV_album VARCHAR(50), " +
                    "MV_size  VARCHAR(50), " +
                    "MV_duration  VARCHAR(50), " +
                    "MV_path  VARCHAR(50));");
                
            db.execSQL("CREATE TABLE Playlist (" +
                       "Playlist_ID INTEGER PRIMARY KEY, " +
                       "Playlist_name  VARCHAR(50));");
                
            db.execSQL("CREATE TABLE Match (" +
                       "Playlist_ID INTEGER, " +
                       "Song_ID  INTEGER," +
                       "PRIMARY KEY (Playlist_id,Song_id));");
                
            db.execSQL("CREATE TABLE STemp (" +
                       "STemp_ID INTEGER PRIMARY KEY, " +
                       "STemp_name VARCHAR(50) , " +
                       "STemp_artist VARCHAR(50) , " +
                       "STemp_album VARCHAR(50), " +
                       "STemp_size  VARCHAR(50), " +
                       "STemp_duration  VARCHAR(50), " +
                       "STemp_path  VARCHAR(50));");
                
            db.execSQL("CREATE TABLE Record (" +
                       "Record_ID INTEGER PRIMARY KEY, " +
                       "Record_name VARCHAR(50) , " +
                       "Record_size  VARCHAR(50), " +
                       "Record_path  VARCHAR(50), " +
                       "Record_date  VARCHAR(50));");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS Song;");
            db.execSQL("DROP TABLE IF EXISTS MV;");
            db.execSQL("DROP TABLE IF EXISTS Playlist;");
            db.execSQL("DROP TABLE IF EXISTS Match;");
            db.execSQL("DROP TABLE IF EXISTS Stemp;");
            db.execSQL("DROP TABLE IF EXISTS Record;");
            onCreate(db);
        }
}