package com.example.pmps;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

public class MainPageActivity extends Activity {
	//介面宣告
	private ImageButton mySong;
	private ImageButton myMV;
	private ImageButton listEdit;
	private ImageButton singingSong;
	private ImageButton myRecord;
	private ImageButton helping;
	private ImageButton aboutPMPS;
	private ImageButton exitPMPS;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);
        
        mySong = (ImageButton)findViewById(R.id.mySong);
        myMV = (ImageButton)findViewById(R.id.myMV);
        listEdit = (ImageButton)findViewById(R.id.listEdit);
        singingSong = (ImageButton)findViewById(R.id.singingSong);
        myRecord = (ImageButton)findViewById(R.id.myRecord);
        helping = (ImageButton)findViewById(R.id.help);
        aboutPMPS = (ImageButton)findViewById(R.id.aboutPMPS);
        exitPMPS = (ImageButton)findViewById(R.id.exitPMPS);

        //按下「我的歌曲」事件
        mySong.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setClass(MainPageActivity.this,MySongActivity.class);				
				startActivity(intent);
        }});
      //按下「MV歡唱」事件
        myMV.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setClass(MainPageActivity.this,MyMVActivity.class);				
				startActivity(intent);
        }});
        //按下「歌單編輯」事件
        listEdit.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setClass(MainPageActivity.this,ListEditActivity.class);				
				startActivity(intent);
		}});
        //按下「歌曲歡唱」事件
        singingSong.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setClass(MainPageActivity.this,SingingActivity.class);				
				startActivity(intent);
		}});
        //按下「我的作品」事件
        myRecord.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setClass(MainPageActivity.this,MyRecordActivity.class);				
				startActivity(intent);
		}});
        //按下「說明」事件
        helping.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				help();
		}});
        //按下「關於」事件
        aboutPMPS.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				about();
		}});  
        //按下「離開」事件
        exitPMPS.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				exit();
		}}); 
    }
    //程式說明
    private void help(){
    	final String[] ListStr ={"圖文並茂版","youtube版"};
    	AlertDialog.Builder helpDialog = new AlertDialog.Builder(this);
    	helpDialog.setTitle("---說明---");
  
    	//建立選擇的事件
    	DialogInterface.OnClickListener ListClick = new DialogInterface.OnClickListener(){
	    	public void onClick(DialogInterface dialog, int which) {
	    		switch(which){
	    		    case 0:
	    		    	Uri uri = Uri.parse("http://pmps2012.blogspot.tw/");  
	    		    	Intent it = new Intent(Intent.ACTION_VIEW, uri);  
	    		    	startActivity(it); 
	    			    break;
	    		    case 1:
	    		    	Uri uri2 = Uri.parse("http://www.youtube.com/watch?v=OVtkcQrA5Uo&feature=plcp");  
	    		    	Intent it2 = new Intent(Intent.ACTION_VIEW, uri2);  
	    		    	startActivity(it2); 
	    			    break;
	    		    default:
	    		        break;
	    		}
	    	}
    	};

    	//建立按下取消什麼事情都不做的事件
    	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener()
    	{
	    	public void onClick(DialogInterface dialog, int which) {
	    		
	    	}

    	}; 
    	
    	helpDialog.setItems(ListStr, ListClick);
    	helpDialog.setNeutralButton("取消",OkClick);
    	helpDialog.show();
    }
    
    //關於全民歌Sing
    private void about(){
    	AlertDialog.Builder aboutDialog = new AlertDialog.Builder(MainPageActivity.this);
		aboutDialog.setTitle("---關於---");
		aboutDialog.setMessage("全民歌Sing \r\nPeople music People sing \r\nBeta Version 2012.10 \r\nDesign by PMPS開發小組 \r\n\r\nP.S 圖片如有侵犯 \r\n請來信:weiwei19910415@gmail.com");
		
		aboutDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    	    public void onClick(DialogInterface arg0, int arg1) {
    	    	
    	     }
    	    });
		aboutDialog.show();
    }
    
	//離開主程式
    private void exit(){
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("確定要離開本程式嗎?")
    	.setPositiveButton("確定", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				//MainPageActivity.this.finish();
				android.os.Process.killProcess(android.os.Process.myPid());
			}})
		.setNegativeButton("取消", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}});
    	AlertDialog about_dialog = builder.create();
    	about_dialog.show();
    }    
}