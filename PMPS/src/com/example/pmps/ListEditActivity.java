package com.example.pmps;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class ListEditActivity extends ListActivity{
	//長按menu選項宣告
	public static final int renameID = Menu.FIRST;
	public static final int deleteID = Menu.FIRST+1;
	public static final int exitID = Menu.FIRST+2;
	//介面宣告
	private ListView listView; 
	private Button newList;
	private Button listBack;
	// SQLite宣告
	MyDBHelper mHelper = null;
	SQLiteDatabase mDB = null;
	Cursor cursor;
	    
	protected String sql;  //抓取sql語法
    protected int currentPosition = 0;
	int listNum = 0 ;
	private List<String> _list = new ArrayList<String>();
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listedit_page);
        
        openDatabase();  //開啟資料庫
        
        listView = (ListView) this.findViewById(android.R.id.list); 
        getlist();       //抓取所有歌單
        
        Toast.makeText(this, "按一下顯示歌單歌曲\r\n長按歌單顯示相關選項", Toast.LENGTH_LONG).show();
         
        newList = (Button)findViewById(R.id.newList);
        newList.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				newlist(); //新增歌單
		}});
        
        listBack = (Button)findViewById(R.id.listBack);
        listBack.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				android.os.Process.killProcess(android.os.Process.myPid());
		}});     
	}
	
	protected void onDestroy() {
        super.onDestroy();
        closeDatabase();
    }
	//開啟資料庫
	private void openDatabase(){
		mHelper = new MyDBHelper(this); 		
    }
    //關閉資料庫
    private void closeDatabase(){
    	mHelper.close();
    }
    //新增歌單
  	public void newlist(){
  		AlertDialog.Builder newlistDialog = new AlertDialog.Builder(ListEditActivity.this);
  		newlistDialog.setTitle("---新增歌單---");
  	    	    
  	    final EditText editText = new EditText(ListEditActivity.this);
  	    newlistDialog.setView(editText);
  	    	
  	    newlistDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
  	    	public void onClick(DialogInterface arg0, int arg1) {
  	    	    String listname = editText.getText().toString();
  	    	    String tempname = null;
  	    	    mDB = mHelper.getWritableDatabase();
  	    	    sql = "SELECT * FROM Playlist;";
  	    	    cursor = mDB.rawQuery(sql, null);
  	    	    cursor.moveToFirst();
  	    	    	
  	    	    //尋找相同名稱
  	    	    for(int i=0;i<cursor.getCount();i++)
  	    	    {
  	    	    	if(listname.equals(cursor.getString(1)))
  	    	    		tempname=cursor.getString(1);
  	    	    	else
  	    	    		cursor.moveToNext();
  	    	    }
  	    	    cursor.close();
  	    	    	
  	    	    //判斷名稱為空白或相同名稱
  	    	    if(listname.equals(""))
  	    	    {
  	    	    	spaceMes();	
  	    	    }
  	    	    else if(listname.equals(tempname))
  	    	    {
  	    	    	sameMes();
  	    	    }
  	    	    else
  	    	    {		    	    	
  		    	    ContentValues values = new ContentValues();
  		    	    	
  		    	    Calendar date = Calendar.getInstance() ;
  		    	    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("MMddHHmmss"); 
  		    	    String currentDate = simpleDateFormat.format(date.getTime());
  		
  		    	    values.put("Playlist_id",currentDate);
  		        	values.put("Playlist_name",listname);
  		        	mDB.insert("Playlist",null,values);	 
  	    	    }
  	    	    	
  	        	getlist();//抓取所有歌單
  	        	mDB.close();
  	    	    }
  	    });
  	    newlistDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
  	    	public void onClick(DialogInterface arg0, int arg1) {}
  	    });
  	    newlistDialog.show();
  	}
    //獲取所有歌單
  	public void getlist(){
  		sql = "SELECT * FROM Playlist;";
  		mDB = mHelper.getWritableDatabase();
  		cursor = mDB.rawQuery(sql, null);
  	    cursor.moveToFirst();
  	    listNum = cursor.getCount();  //抓取歌單數量
  	        
  	    ArrayList<HashMap<String, String>> searchList = new ArrayList<HashMap<String, String>>();
  	    for(int i=0;i<cursor.getCount();i++)
  	    {
  	        _list.add(cursor.getString(0));
  	        HashMap<String, String> searchMap = new HashMap<String, String>();
  	        searchMap.put("listItem", cursor.getString(1));
  	        searchList.add(searchMap);
  	        cursor.moveToNext();
  	    }
  	    cursor.close();
  	        
  	    SimpleAdapter mSearch = new SimpleAdapter(this,searchList,R.layout.listedit_item,new String[] {"listItem"}, new int[] {R.id.listItem});
  	    listView.setAdapter(mSearch);
  	    registerForContextMenu(listView); 
  	}
  	//顯示空白錯誤訊息
  	public void spaceMes(){
  		AlertDialog.Builder wrongDialog = new AlertDialog.Builder(ListEditActivity.this);
  		wrongDialog.setTitle("---錯誤---");
  		wrongDialog.setMessage("歌單名稱不可為空白");
  			
  		wrongDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
  	        public void onClick(DialogInterface arg0, int arg1) {}
  	    });
  		wrongDialog.show();
  	}
  	//顯示相同名稱播放清單錯誤訊息
  	public void sameMes(){
  	    AlertDialog.Builder sameDialog = new AlertDialog.Builder(ListEditActivity.this);
  		sameDialog.setTitle("---錯誤---");
  		sameDialog.setMessage("有相同名稱的播放清單，請重新輸入！");
  			
  		sameDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
  	    	public void onClick(DialogInterface arg0, int arg1) {}
  	    });
  		sameDialog.show();
  	}
  
  	protected void onListItemClick(ListView l, View v, int position, long id) {
	    currentPosition = position;
			
		mDB = mHelper.getWritableDatabase();
	    sql = "SELECT * FROM Playlist;";
	    cursor = mDB.rawQuery(sql, null);
	    cursor.moveToPosition(currentPosition);
				
		Intent intent = new Intent();
		intent.setClass(ListEditActivity.this,ListSongActivity.class);	
			
		Bundle bundle = new Bundle();
		bundle.putString("Playlist_id", cursor.getString(0));
		intent.putExtras(bundle);
			
		startActivity(intent);	
	}
		
	public void onListLongClick(ListView l, View v,int position, long id) {
		currentPosition = position;
	}
		
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		menu.setHeaderTitle("Menu");
		currentPosition = ((AdapterContextMenuInfo)menuInfo).position;
			
		menu.add(0,renameID,0,"重新命名");
		menu.add(0,deleteID,0,"刪除歌單");
		menu.add(0,exitID,0,"返回");	
	}
		
    public boolean onContextItemSelected(MenuItem item) {
		super.onContextItemSelected(item);
		switch( item.getItemId() )
		{
			case renameID:
			    AlertDialog.Builder editDialog = new AlertDialog.Builder(ListEditActivity.this);
			    editDialog.setTitle("---重新命名---");
			    	    
			    final EditText editText = new EditText(ListEditActivity.this);
			    editDialog.setView(editText);
			          
			    editDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			    	public void onClick(DialogInterface arg0, int arg1) {
			    	    String listname = editText.getText().toString();
			    	    String tempname = null;
			    	    mDB = mHelper.getWritableDatabase();
			    	    sql = "SELECT * FROM Playlist;";
			    	    cursor = mDB.rawQuery(sql, null);
			    	    cursor.moveToFirst();
			    	    	
			    	    //尋找相同名稱
			    	    for(int i=0;i<cursor.getCount();i++)
			    	    {
			    	    	if(listname.equals(cursor.getString(1)))
			    	    		tempname=cursor.getString(1);
			    	    	else
			    	    		cursor.moveToNext();
			    	    }
			    	    cursor.close();
			    	    	
			    	    //判斷名稱為空白或相同名稱
			    	    if(listname.equals("")){
			    	    	spaceMes();	
			    	    }
			    	    else if(listname.equals(tempname))
			    	    {
			    	    	sameMes();
			    	    }
			    	    else{
			    	    	sql = "SELECT * FROM Playlist;";
			    	    	mDB = mHelper.getWritableDatabase();
			    	    	cursor = mDB.rawQuery(sql, null);
			    	    	cursor.moveToPosition(currentPosition);
			    	    	String currentid = cursor.getString(0);
			    	    	mDB.execSQL("Update Playlist SET Playlist_name= '"+listname+"' WHERE Playlist_id = "+ currentid +";");
			    	    }
			    	    getlist();
			    	 }
			    });
			    editDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			    	    public void onClick(DialogInterface arg0, int arg1) {}
			    });
			    editDialog.show();
				break;
			case deleteID:
			    AlertDialog.Builder deleteDialog = new AlertDialog.Builder(ListEditActivity.this);
			    deleteDialog.setTitle("---刪除歌單---");
			    deleteDialog.setMessage("確定要刪除歌單嗎？");
			    	
			    deleteDialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						sql = "SELECT * FROM Playlist;";
			    		mDB = mHelper.getWritableDatabase();
			    		cursor = mDB.rawQuery(sql, null);
			    		cursor.moveToPosition(currentPosition);
							
			    		mDB.execSQL("DELETE FROM Playlist WHERE Playlist_name="+"'"+cursor.getString(1)+"'");
			    		getlist();
			    			
					}			    		
			    });
			    deleteDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {}
			    });
			    deleteDialog.show();			    	
				break;    
			case exitID:
				break;
		}
		return true;
	}
		
	public void onItemClick(ListView l, View v, int position, long id) {

	}
}
