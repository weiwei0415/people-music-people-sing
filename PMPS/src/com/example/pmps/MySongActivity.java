package com.example.pmps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MySongActivity extends ListActivity {
	//長按menu選項宣告
	public static final int listenID = Menu.FIRST;
	public static final int listID = Menu.FIRST+1;
	public static final int dataID = Menu.FIRST+2;
	public static final int exitID = Menu.FIRST+3;
	//介面宣告
	private ListView listView;  
	private EditText keyword;
	private Button songRefresh;
	private Button songBack;
	private ImageButton Speech;
	
	//儲存音樂資訊
	protected List<String> title = new ArrayList<String>();
	protected List<String> artist = new ArrayList<String>();
	protected List<String> album = new ArrayList<String>();
	protected List<String> size = new ArrayList<String>();
	protected List<String> path = new ArrayList<String>();
	protected List<String> duration = new ArrayList<String>();
	//儲存歌曲和歌單的id
	protected List<String> _songid = new ArrayList<String>();
	protected List<String> _listid = new ArrayList<String>();
	// SQLite宣告
    MyDBHelper mHelper = null;
    SQLiteDatabase mDB = null;
    Cursor cursor = null;
    
    private int totalCount; //計算SD卡內歌曲數量
	private String sql;  //抓取sql語法
	private String key;  //抓取EditText內文字
	private String dispStr = ""; //暫存歌曲資訊
	protected String songPath; //抓取目前選取的音樂位址
	protected int currentPosition = 0;  //抓取Listview目前選取的位置
	private static final int RQS_VOICE_RECOGNITION = 1;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.songedit_page);		
		
		openDatabase();  //開啟資料庫
		cleanList();	
		getSongInfo();
		getSongInfo_SQL();
		
		Toast.makeText(this, "長按歌曲顯示相關選項", Toast.LENGTH_LONG).show();
		
		songRefresh = (Button)findViewById(R.id.songButton1);
		songRefresh.setText("重新整理");
		songRefresh.setCompoundDrawablesWithIntrinsicBounds(R.drawable.av_replay, 0, 0, 0);		
		songRefresh.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				cleanList();	
				getSongInfo();
				getSongInfo_SQL();
				listSong();
        }});
        //語音辨識
		Speech = (ImageButton)findViewById(R.id.Speech);
		Speech.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View v) {
				// TODO 自動產生的方法 Stub
				Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			       intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
			           RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			       intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
			           "說出想找的歌名:D");
			       startActivityForResult(intent, RQS_VOICE_RECOGNITION);
			}
		}
		 
		 
		);
		
			
		
		
        songBack = (Button)findViewById(R.id.songButton2);
        songBack.setOnClickListener(new ImageButton.OnClickListener(){

			public void onClick(View arg0) {
				android.os.Process.killProcess(android.os.Process.myPid());
        }});
		
		listView = (ListView) this.findViewById(android.R.id.list);
		listSong();
		
		//依關鍵字輸入去做搜尋
        keyword = (EditText)findViewById(R.id.keyword);
        keyword.addTextChangedListener( new TextWatcher(){
			public void afterTextChanged(Editable arg0) {
								
			}
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			
			}
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				    searchSong();
			}});
	}
	//語音辨識結果
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		  // TODO Auto-generated method stub
		  if(requestCode == RQS_VOICE_RECOGNITION){
		   if(resultCode == RESULT_OK){
		 
		    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
		    String firstMatch = (String)result.get(0);
		    keyword.setText(firstMatch);
		   }
		  }
		   
		 
		 }
	protected void onDestroy() {
        super.onDestroy();
        closeDatabase();
    }
	//開啟資料庫
	protected void openDatabase(){
		mHelper = new MyDBHelper(this); 	
    }
    //關閉資料庫
    protected void closeDatabase(){
    	mHelper.close();
    }
    //抓取歌曲資訊
    public void getSongInfo(){				 
		Context ctx = MySongActivity.this ;
		ContentResolver resolver = ctx.getContentResolver();
		
		Cursor c = resolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, "_data LIKE '%.mp3'", null,null);			
		c.moveToFirst();
		totalCount = c.getCount();
		
		for ( int i = 0 ; i < totalCount; i++ ){
			int index = c.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
			String src = c.getString(index);
			title.add(src);
				
			index = c.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);
			src = c.getString(index);
			artist.add(src);
				
			index = c.getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM);
			src = c.getString(index);
			album.add(src);
							
			index = c.getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE);
			src = c.getString(index);
			int parseSize = Integer.parseInt(src);
			parseSize = parseSize/1024;
			src = Integer.toString(parseSize);
			size.add(src);
					
			index = c.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
			src = c.getString(index);
		    path.add(src);		  

			index = c.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION);
			int length = c.getInt(index);
			length = length/1000; // length in sec
			int sec = length%60;
			length = length-sec;
			int min = length/60;
			if (sec < 10){
				duration.add(min +":0"+ sec);			    
			}
			else{
				duration.add(min +":"+ sec);
			}
			c.moveToNext();
		}
		c.close();
	}
    //將歌曲資訊儲存到"Song"table
    public void getSongInfo_SQL(){
        mDB = mHelper.getWritableDatabase();
        
        mDB.execSQL("DROP TABLE IF EXISTS Song;");
        mDB.execSQL("CREATE TABLE Song (" +
                "Song_ID INTEGER PRIMARY KEY, " +
                "Song_name VARCHAR(50) , " +
                "Song_artist VARCHAR(50) , " +
                "Song_album VARCHAR(50), " +
                "Song_size  VARCHAR(50), " +
                "Song_duration  VARCHAR(50), " +
                "Song_path  VARCHAR(50));");
        
        ContentValues values = new ContentValues();
    	for ( int j = 0; j < totalCount; j++ ){  
    		values.put("Song_id",j);
    		values.put("Song_name",title.get(j));
    		values.put("Song_artist",artist.get(j));
    		values.put("Song_album",album.get(j));
    		values.put("Song_size",size.get(j));
    		values.put("Song_duration",duration.get(j));
    		values.put("Song_path",path.get(j));
    		mDB.insert("Song",null,values);	    		
    	}
    	mDB.close();
    }
    //將歌曲顯示在listview上
    public void listSong(){
    	mDB = mHelper.getWritableDatabase();  
        sql = "SELECT * FROM Song;";
        cursor = mDB.rawQuery(sql, null);
        cursor.moveToFirst();
        
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        for(int i=0;i<cursor.getCount();i++)
        {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("songTitle", cursor.getString(1));  
            map.put("songText", cursor.getString(2));
            map.put("songText2", cursor.getString(5));
            mylist.add(map);
            cursor.moveToNext();
        }
        cursor.close();
        mDB.close();
        
      //使用SimpleAdapter建立歌曲列表項目，並且註冊長按功能
        SimpleAdapter mSchedule = new SimpleAdapter(this,mylist,R.layout.songedit_item,new String[] {"songTitle", "songText","songText2"}, new int[] {R.id.songTitle,R.id.songText1,R.id.songText2});
        listView.setAdapter(mSchedule);
        registerForContextMenu(listView); 
    } 
    //依關鍵字尋找歌曲
    public void searchSong(){
		key = keyword.getText().toString();
		sql = "SELECT * FROM Song WHERE Song_name LIKE '%"+ key +"%';";
		mDB = mHelper.getWritableDatabase();  
		cursor = mDB.rawQuery(sql, null);
		cursor.moveToFirst();		
		cleanList(); //每次搜尋時，歌曲位址重新記錄
		
        ArrayList<HashMap<String, String>> searchList = new ArrayList<HashMap<String, String>>();
        for(int i=0;i<cursor.getCount();i++)
        {
            HashMap<String, String> searchMap = new HashMap<String, String>();
            searchMap.put("songTitle", cursor.getString(1)); 
            searchMap.put("songText", cursor.getString(2));
            searchMap.put("songText2", cursor.getString(5));
            
            title.add(cursor.getString(1));
        	artist.add(cursor.getString(2));
        	album.add(cursor.getString(3));
        	size.add(cursor.getString(4));
        	duration.add(cursor.getString(5));
            path.add(cursor.getString(6));
            
            searchList.add(searchMap);
            cursor.moveToNext();
        }
        cursor.close();
        mDB.close();
        
        SimpleAdapter mSearch = new SimpleAdapter(this,searchList,R.layout.songedit_item,new String[] {"songTitle", "songText","songText2"}, new int[] {R.id.songTitle,R.id.songText1,R.id.songText2});
        listView.setAdapter(mSearch);
        registerForContextMenu(listView); 
	}
    //顯示歌曲資訊
    public void printSongInfo(String sql){
    	mDB = mHelper.getWritableDatabase();  
    	cursor = mDB.rawQuery(sql, null);
    	cursor.moveToPosition(currentPosition);
    	
    	dispStr = "歌曲名稱："+cursor.getString(1)+ "\n";
    	dispStr += "演出者："+cursor.getString(2)+ "\n";
    	dispStr += "專輯："+cursor.getString(3)+ "\n";
    	dispStr += "大小："+cursor.getString(4)+ "KB\n";
    	dispStr += "長度："+cursor.getString(5)+ "\n";
    	dispStr += "路徑："+cursor.getString(6)+ "\n";
    	
    	cursor.close();
    	mDB.close();
    	new AlertDialog.Builder(MySongActivity.this).setMessage(dispStr).create().show();
    }
    //顯示歌曲重複加入歌單的錯誤訊息
    public void sameIdMes(){
		AlertDialog.Builder sameDialog = new AlertDialog.Builder(MySongActivity.this);
		sameDialog.setTitle("---錯誤---");
		sameDialog.setMessage("歌單內已有歌曲");
		
		sameDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    	    public void onClick(DialogInterface arg0, int arg1) {
    	    	
    	     }
    	    });
		sameDialog.show();
	}
    //清除所有List內資料
    public void cleanList(){
    	title.clear();
    	artist.clear();
    	album.clear();
    	size.clear();
    	path.clear();
    	duration.clear();
    }   
    //建立Menu選項的事件
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		menu.setHeaderTitle("Menu");
		currentPosition = ((AdapterContextMenuInfo)menuInfo).position;
		songPath = path.get(currentPosition);
		
		menu.add(0,listenID,0,"試聽");
		menu.add(0,listID,0,"加入歌單");
		menu.add(0,dataID,0,"歌曲資訊");
		menu.add(0,exitID,0,"返回");
	}
    
    //Menu選項被選時執行的事件
    public boolean onContextItemSelected(MenuItem item) {
		super.onContextItemSelected(item);
		switch( item.getItemId() )
		{
		    case listenID:
		    	Intent intent=new Intent();
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.setAction(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse("file://"+songPath), "audio");
				startActivity(intent);    	    
		        break;
		    case listID:		    	
		    	AlertDialog.Builder shareDialog = new AlertDialog.Builder(MySongActivity.this);
		    	shareDialog.setTitle("---加入歌單---");
		    	
		    	String pSql = "SELECT * FROM Playlist;";
		    	mDB = mHelper.getWritableDatabase(); 
		    	cursor = mDB.rawQuery(pSql, null);
		    	cursor.moveToFirst();
		        
		        String[] choices= new String [cursor.getCount()];
		        for(int i=0;i<cursor.getCount();i++)
		        {
		        	_listid.add(cursor.getString(0));
		        	choices[i] = cursor.getString(1);
		        	cursor.moveToNext();
		        }
		        cursor.close();
		        mDB.close();
		        	      
		        mDB = mHelper.getWritableDatabase(); 
		        cursor = mDB.rawQuery(sql, null);
		        cursor.moveToFirst();
		        
		        for(int i=0;i<cursor.getCount();i++)
		        {
		        	_songid.add(cursor.getString(0));
		        	cursor.moveToNext();
		        }
		        cursor.close();
		        mDB.close();
		        
		    	shareDialog.setItems(choices, ListClick);	    	
		    	shareDialog.show();		    	
			    break;    
		    case dataID:
		    	printSongInfo(sql);		    	
				break;	
		    case exitID:
			    break;
		}
		return true;
	}    
    protected void onListItemClick(ListView l, View v, int position, long id) {
		 currentPosition = position;
		 songPath = path.get(currentPosition);
		 Toast.makeText(this, "長按歌曲顯示相關選項", Toast.LENGTH_LONG).show();
	}	
	public void onListLongClick(ListView l, View v,int position, long id) {
		 currentPosition = position;
		 songPath = path.get(currentPosition);
	}
	
	DialogInterface.OnClickListener ListClick = new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
			String songid = _songid.get(currentPosition);
			String tempid = null;
			
			String mSql = "SELECT * FROM Match;";
			mDB = mHelper.getWritableDatabase();   
			cursor = mDB.rawQuery(mSql, null);
			cursor.moveToFirst();
	        
	        for(int i=0;i<cursor.getCount();i++)
	        {
	    		if(songid.equals(cursor.getString(1)) && _listid.get(which).equals(cursor.getString(0)))
	    			tempid=cursor.getString(1);
	    		else
	    			cursor.moveToNext();
	        }
	        cursor.close();
	        
	        mDB = mHelper.getWritableDatabase();  
	        
	        if(songid.equals(tempid))
	    	{
	    		sameIdMes();
	    		mDB.close();
	    	}
	        else{
		    	ContentValues values = new ContentValues();
		    	values.put("Playlist_id",_listid.get(which));
	    		values.put("Song_id",_songid.get(currentPosition));
	    		mDB.insert("Match",null,values);
	    		mDB.close();
	    		
	    		AlertDialog.Builder insertokDialog = new AlertDialog.Builder(MySongActivity.this);
	    		insertokDialog.setTitle("---新增成功---");
	    		insertokDialog.setMessage("新增成功");
	    		
	    		insertokDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface arg0, int arg1) {
	    				
	    			}
	    		});
	    		
	    		insertokDialog.show();
	        } 

		}
	};	
	public void onItemClick(ListView l, View v, int position, long id) {
		 
	}    
}
