package com.example.pmps;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MyRecordActivity extends ListActivity{
	//長按menu選項宣告
	public static final int playID = Menu.FIRST;
	public static final int shareID = Menu.FIRST+1;
	public static final int deleteID = Menu.FIRST+2;
	public static final int exitID = Menu.FIRST+3;
	//SQLite宣告
	MyDBHelper rHelper = null;
	SQLiteDatabase rDB = null;
	Cursor rCursor = null;
	//介面宣告
    private ListView listView; 
    private Button refresh;
	private Button myrecordBack;
    
	private String recordPath;
	private String sql;  //抓取sql語法
	protected int currentPosition = 0;	
	private List<String> recordList=new ArrayList<String>();
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myrecord_page);
        
        openDatabase();  //開啟資料庫    
        
        listView = (ListView) this.findViewById(android.R.id.list);          
        createRecordlist();
        
        Toast.makeText(this, "長按歌曲顯示相關選項", Toast.LENGTH_LONG).show();
        
        refresh = (Button)findViewById(R.id.refresh);
        refresh.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
    	        createRecordlist();
        }});
        
        myrecordBack = (Button)findViewById(R.id.myrecordBack);
        myrecordBack.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				android.os.Process.killProcess(android.os.Process.myPid());
        }});             
	}
	
	protected void onDestroy() {
        super.onDestroy();
        closeDatabase();
    }
	
	private void openDatabase(){
		rHelper = new MyDBHelper(this); 
    }

    private void closeDatabase(){
    	rHelper.close();
    }
    
	public void createRecordlist(){
		rDB = rHelper.getWritableDatabase();
        sql = "SELECT * FROM Record;";
        rCursor = rDB.rawQuery(sql, null);
        rCursor.moveToFirst();
        
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        for(int i=0;i<rCursor.getCount();i++)
        {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("recordTitle", rCursor.getString(1));
            map.put("recordText1", rCursor.getString(2));
            map.put("recordText2", rCursor.getString(4));
            recordList.add(rCursor.getString(3));
            mylist.add(map);
            rCursor.moveToNext();
        }
        rCursor.close();
        rDB.close();
        
        //使用SimpleAdapter建立歌曲列表項目，並且註冊長按功能
        SimpleAdapter mSchedule = new SimpleAdapter(this,mylist,R.layout.myrecord_item,new String[] {"recordTitle", "recordText1","recordText2"}, new int[] {R.id.recordTitle,R.id.recordText1,R.id.recordText2});
        listView.setAdapter(mSchedule);
        registerForContextMenu(listView);
	}
	
	public void deleteRecord(){
		rDB = rHelper.getWritableDatabase();
		sql = "DELETE FROM Record WHERE Record_path = '" + recordPath + "';";
		rDB.execSQL(sql);
		rDB.close();
	}
	
	public boolean onContextItemSelected(MenuItem item) {		
		super.onContextItemSelected(item);
		switch( item.getItemId() )
		{
		    case playID:
		    	Intent intent=new Intent();
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.setAction(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse("file://"+recordList.get(currentPosition)), "audio");
				startActivity(intent);    	    
		        break;
		    case shareID:
		    	Intent it = new Intent(android.content.Intent.ACTION_SEND);   
				it.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+recordPath));  
				it.setType("audio/*");  
			    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  
				startActivity(Intent.createChooser(it, "分享至"));  
			    break;    		    
		    case deleteID:
		    	AlertDialog.Builder deleteDialog = new AlertDialog.Builder(MyRecordActivity.this);
		    	deleteDialog.setTitle("---刪除檔案---");
		    	deleteDialog.setMessage("確定要刪除檔案嗎?");
		    	deleteDialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
		    	    public void onClick(DialogInterface arg0, int arg1) {
		    	    	File file = new File(recordPath);
		    	    	file.delete();
		    	    	
		    	    	deleteRecord();		    	    	  	
		    	        createRecordlist();		    	    	
		    	     }
		    	});
		    	deleteDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
		    	    public void onClick(DialogInterface arg0, int arg1) {}
		    	});
		    	deleteDialog.show();  
				break;	
		    case exitID:
			    break;
		}
		return true;
	}

	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		menu.setHeaderTitle("Menu");
		currentPosition = ((AdapterContextMenuInfo)menuInfo).position;
		recordPath = recordList.get(currentPosition);
		
		menu.add(0,playID,0,"播放");
		menu.add(0,shareID,0,"分享");
		menu.add(0,deleteID,0,"刪除");
		menu.add(0,exitID,0,"返回");
		
	}
	
	 protected void onListItemClick(ListView l, View v, int position, long id) {
		 currentPosition = position;
		 recordPath = recordList.get(currentPosition);
		 Toast.makeText(this, "長按歌曲顯示相關選項", Toast.LENGTH_LONG).show();
	 }
	
	public void onListLongClick(ListView l, View v,int position, long id) {
		 currentPosition = position;
		 recordPath = recordList.get(currentPosition);
	}
}
