package com.example.pmps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MyMVActivity extends ListActivity {
	//長按menu選項宣告
	public static final int playID = Menu.FIRST;
	public static final int dataID = Menu.FIRST+1;
	public static final int exitID = Menu.FIRST+2;
	//介面宣告
	private ListView listView;  
	private EditText MVkeyword;
	private Button MVRefresh;
	private Button MVBack;
	private ImageButton MVSpeech;
	
	//儲存音樂資訊
	protected List<String> title = new ArrayList<String>();
	protected List<String> artist = new ArrayList<String>();
	protected List<String> album = new ArrayList<String>();
	protected List<String> size = new ArrayList<String>();
	protected List<String> path = new ArrayList<String>();
	protected List<String> duration = new ArrayList<String>();
	//儲存歌曲和歌單的id
	protected List<String> _songid = new ArrayList<String>();
	protected List<String> _listid = new ArrayList<String>();
	// SQLite宣告
    MyDBHelper mHelper = null;
    SQLiteDatabase mDB = null;
    Cursor cursor = null;
    
    private int totalCount; //計算SD卡內歌曲數量
	private String sql;  //抓取sql語法
	private String key;  //抓取EditText內文字
	private String dispStr = ""; //暫存歌曲資訊
	protected String songPath; //抓取目前選取的音樂位址
	protected int currentPosition = 0;  //抓取Listview目前選取的位置
	private static final int RQS_VOICE_RECOGNITION = 1;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mvsinging_page);		
		
		openDatabase();  //開啟資料庫
		cleanList();	
		getMVInfo();
		getMVInfo_SQL();
		
		Toast.makeText(this, "長按MV顯示相關選項", Toast.LENGTH_LONG).show();
		
		MVRefresh = (Button)findViewById(R.id.MVButton1);
		MVRefresh.setText("重新整理");
		MVRefresh.setCompoundDrawablesWithIntrinsicBounds(R.drawable.av_replay, 0, 0, 0);		
		MVRefresh.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				cleanList();	
				getMVInfo();
				getMVInfo_SQL();
				listMV();
        }});
        //語音辨識
		MVSpeech = (ImageButton)findViewById(R.id.mvspeech);
		MVSpeech.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View v) {
				// TODO 自動產生的方法 Stub
				Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			       intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
			           RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			       intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
			           "說出想找的歌名:D");
			       startActivityForResult(intent, RQS_VOICE_RECOGNITION);
			}
		}
		 
		 
		);
		
			
		
		
        MVBack = (Button)findViewById(R.id.MVButton2);
        MVBack.setOnClickListener(new ImageButton.OnClickListener(){

			public void onClick(View arg0) {
				android.os.Process.killProcess(android.os.Process.myPid());
        }});
		
		listView = (ListView) this.findViewById(android.R.id.list);
		listMV();
		
		//依關鍵字輸入去做搜尋
        MVkeyword = (EditText)findViewById(R.id.mvkeyword);
        MVkeyword.addTextChangedListener( new TextWatcher(){
			public void afterTextChanged(Editable arg0) {
								
			}
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			
			}
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				    searchMV();
			}});
	}
	//語音辨識結果
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		  // TODO Auto-generated method stub
		  if(requestCode == RQS_VOICE_RECOGNITION){
		   if(resultCode == RESULT_OK){
		 
		    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
		    String firstMatch = (String)result.get(0);
		    MVkeyword.setText(firstMatch);
		   }
		  }
		   
		 
		 }
	protected void onDestroy() {
        super.onDestroy();
        closeDatabase();
    }
	//開啟資料庫
	protected void openDatabase(){
		mHelper = new MyDBHelper(this); 	
    }
    //關閉資料庫
    protected void closeDatabase(){
    	mHelper.close();
    }
    //抓取歌曲資訊
    public void getMVInfo(){				 
		Context ctx = MyMVActivity.this ;
		ContentResolver resolver = ctx.getContentResolver();
		
		Cursor c = resolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, "_data LIKE '%.3gp'", null,null);			
		c.moveToFirst();
		totalCount = c.getCount();
		
		for ( int i = 0 ; i < totalCount; i++ ){
			int index = c.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE);
			String src = c.getString(index);
			title.add(src);
				
			index = c.getColumnIndexOrThrow(MediaStore.Video.Media.ARTIST);
			src = c.getString(index);
			artist.add(src);
				
			index = c.getColumnIndexOrThrow(MediaStore.Video.Media.ALBUM);
			src = c.getString(index);
			album.add(src);
							
			index = c.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE);
			src = c.getString(index);
			int parseSize = Integer.parseInt(src);
			parseSize = parseSize/1024;
			src = Integer.toString(parseSize);
			size.add(src);
					
			index = c.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
			src = c.getString(index);
		    path.add(src);		  

			index = c.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION);
			int length = c.getInt(index);
			length = length/1000; // length in sec
			int sec = length%60;
			length = length-sec;
			int min = length/60;
			if (sec < 10){
				duration.add(min +":0"+ sec);			    
			}
			else{
				duration.add(min +":"+ sec);
			}
			c.moveToNext();
		}
		c.close();
	}
    //將MV資訊儲存到"MV"table
    public void getMVInfo_SQL(){
        mDB = mHelper.getWritableDatabase();
        
        mDB.execSQL("DROP TABLE IF EXISTS MV;");
        mDB.execSQL("CREATE TABLE MV (" +
                "MV_ID INTEGER PRIMARY KEY, " +
                "MV_name VARCHAR(50) , " +
                "MV_artist VARCHAR(50) , " +
                "MV_album VARCHAR(50), " +
                "MV_size  VARCHAR(50), " +
                "MV_duration  VARCHAR(50), " +
                "MV_path  VARCHAR(50));");
        
        ContentValues values = new ContentValues();
    	for ( int j = 0; j < totalCount; j++ ){  
    		values.put("MV_id",j);
    		values.put("MV_name",title.get(j));
    		values.put("MV_artist",artist.get(j));
    		values.put("MV_album",album.get(j));
    		values.put("MV_size",size.get(j));
    		values.put("MV_duration",duration.get(j));
    		values.put("MV_path",path.get(j));
    		mDB.insert("MV",null,values);	    		
    	}
    	mDB.close();
    }
    //將MV顯示在listview上
    public void listMV(){
    	mDB = mHelper.getWritableDatabase();  
        sql = "SELECT * FROM MV;";
        cursor = mDB.rawQuery(sql, null);
        cursor.moveToFirst();
        
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        for(int i=0;i<cursor.getCount();i++)
        {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("songTitle", cursor.getString(1));  
            map.put("songText", cursor.getString(2));
            map.put("songText2", cursor.getString(5));
            mylist.add(map);
            cursor.moveToNext();
        }
        cursor.close();
        mDB.close();
        
      //使用SimpleAdapter建立歌曲列表項目，並且註冊長按功能
        SimpleAdapter mSchedule = new SimpleAdapter(this,mylist,R.layout.songedit_item,new String[] {"songTitle", "songText","songText2"}, new int[] {R.id.songTitle,R.id.songText1,R.id.songText2});
        listView.setAdapter(mSchedule);
        registerForContextMenu(listView); 
    } 
    //依關鍵字尋找MV
    public void searchMV(){
		key = MVkeyword.getText().toString();
		sql = "SELECT * FROM MV WHERE MV_name LIKE '%"+ key +"%';";
		mDB = mHelper.getWritableDatabase();  
		cursor = mDB.rawQuery(sql, null);
		cursor.moveToFirst();		
		cleanList(); //每次搜尋時，歌曲位址重新記錄
		
        ArrayList<HashMap<String, String>> searchList = new ArrayList<HashMap<String, String>>();
        for(int i=0;i<cursor.getCount();i++)
        {
            HashMap<String, String> searchMap = new HashMap<String, String>();
            searchMap.put("songTitle", cursor.getString(1)); 
            searchMap.put("songText", cursor.getString(2));
            searchMap.put("songText2", cursor.getString(5));
            
            title.add(cursor.getString(1));
        	artist.add(cursor.getString(2));
        	album.add(cursor.getString(3));
        	size.add(cursor.getString(4));
        	duration.add(cursor.getString(5));
            path.add(cursor.getString(6));
            
            searchList.add(searchMap);
            cursor.moveToNext();
        }
        cursor.close();
        mDB.close();
        
        SimpleAdapter mSearch = new SimpleAdapter(this,searchList,R.layout.songedit_item,new String[] {"songTitle", "songText","songText2"}, new int[] {R.id.songTitle,R.id.songText1,R.id.songText2});
        listView.setAdapter(mSearch);
        registerForContextMenu(listView); 
	}
    //顯示歌曲資訊
    public void printMVInfo(String sql){
    	mDB = mHelper.getWritableDatabase();  
    	cursor = mDB.rawQuery(sql, null);
    	cursor.moveToPosition(currentPosition);
    	
    	dispStr = "歌曲名稱："+cursor.getString(1)+ "\n";
    	dispStr += "演出者："+cursor.getString(2)+ "\n";
    	dispStr += "專輯："+cursor.getString(3)+ "\n";
    	dispStr += "大小："+cursor.getString(4)+ "KB\n";
    	dispStr += "長度："+cursor.getString(5)+ "\n";
    	dispStr += "路徑："+cursor.getString(6)+ "\n";
    	
    	cursor.close();
    	mDB.close();
    	new AlertDialog.Builder(MyMVActivity.this).setMessage(dispStr).create().show();
    }

    //清除所有List內資料
    public void cleanList(){
    	title.clear();
    	artist.clear();
    	album.clear();
    	size.clear();
    	path.clear();
    	duration.clear();
    }   
    //建立Menu選項的事件
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		menu.setHeaderTitle("Menu");
		currentPosition = ((AdapterContextMenuInfo)menuInfo).position;
		songPath = path.get(currentPosition);
		
		menu.add(0,playID,0,"MV播放");
		menu.add(0,dataID,0,"歌曲資訊");
		menu.add(0,exitID,0,"返回");
	}
    
    //Menu選項被選時執行的事件
    public boolean onContextItemSelected(MenuItem item) {
		super.onContextItemSelected(item);
		switch( item.getItemId() )
		{
		    case playID:
		    	Intent intent = new Intent(); //建立一個intent
	    	    intent.setClass(MyMVActivity.this, MVPlayer.class);
	    	
	    	    Bundle bundle = new Bundle();

    	        bundle.putString("mPath",path.get(currentPosition));

    	        intent.putExtras(bundle);
    	        startActivity(intent);  	    
		        break;
		    case dataID:
		    	printMVInfo(sql);		    	
				break;	
		    case exitID:
			    break;
		}
		return true;
	}    
    protected void onListItemClick(ListView l, View v, int position, long id) {
		 currentPosition = position;
		 songPath = path.get(currentPosition);
		 Toast.makeText(this, "長按MV顯示相關選項", Toast.LENGTH_LONG).show();
	}	
	public void onListLongClick(ListView l, View v,int position, long id) {
		 currentPosition = position;
		 songPath = path.get(currentPosition);
	}
	
	public void onItemClick(ListView l, View v, int position, long id) {
		 
	}    
}
