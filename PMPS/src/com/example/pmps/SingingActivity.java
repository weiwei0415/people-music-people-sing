package com.example.pmps;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TabHost;

public class SingingActivity extends TabActivity{
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
   
        TabHost tabHost = getTabHost();
		LayoutInflater.from(this).inflate(R.layout.singing_page,
				tabHost.getTabContentView(), true);

		tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator("�̺q��",getResources().getDrawable(R.drawable.collections_collection1))
				.setContent(new Intent(this, SongSinging.class)));
		tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator("�̺q��",getResources().getDrawable(R.drawable.collections_collection2))
				.setContent(new Intent(this, ListSinging.class)));
        
    }
}
