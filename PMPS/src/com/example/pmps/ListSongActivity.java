package com.example.pmps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.TextView;

public class ListSongActivity extends ListActivity{
	//長按menu選項宣告
	public static final int dataID = Menu.FIRST;
	public static final int deleteID = Menu.FIRST+1;
	public static final int exitID = Menu.FIRST+2;
	//介面宣告
	private ListView listView;
	private EditText keyword;
	private Button newSong;
	private Button listsongBack;
	private TextView mTitle;
	// SQLite宣告
	MyDBHelper mHelper = null;
	SQLiteDatabase mDB = null;
	Cursor cursor = null;
	
	private String Playlist_id; //抓取選取的歌單id
	private String sql;  //抓取sql語法
	private String dispStr = ""; //暫存歌曲資訊
	private String key = null;  //抓取EditText內文字
	protected int currentPosition = 0;  //抓取Listview目前選取的位置
	protected List<String> _song = new ArrayList<String>(); //抓取歌單內歌曲id
	protected List<String> _tempSong = new ArrayList<String>(); //抓取歌單內歌曲id
	protected List<String> title = new ArrayList<String>();  //抓取歌單內歌曲名稱
	protected List<String> artist = new ArrayList<String>();  //抓取歌單內歌曲演出者
	protected List<String> path = new ArrayList<String>();  //抓取歌單內歌曲路徑
	
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
        setContentView(R.layout.songedit_page);
        
        openDatabase();  //開啟資料庫
        
        Bundle bundle = this.getIntent().getExtras();
        Playlist_id = bundle.getString("Playlist_id");  
        
        listView = (ListView) this.findViewById(android.R.id.list);
        getMatch();
        getSong();
        
        Toast.makeText(this, "長按歌曲顯示相關選項", Toast.LENGTH_LONG).show();
        
        mTitle = (TextView)findViewById(R.id.textView1);
        mTitle.setText("歌單歌曲");
        
        newSong = (Button)findViewById(R.id.songButton1);
        newSong.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();/*建立一個intent*/
				intent.setClass(ListSongActivity.this,MySongActivity.class);				
				startActivity(intent);
        }});
        
        listsongBack = (Button)findViewById(R.id.songButton2);
        listsongBack.setOnClickListener(new ImageButton.OnClickListener(){
			public void onClick(View arg0) {
				android.os.Process.killProcess(android.os.Process.myPid());
        }});
        
        keyword = (EditText)findViewById(R.id.keyword);
        keyword.addTextChangedListener( new TextWatcher(){
			public void afterTextChanged(Editable arg0) {}
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {}
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				searchSong();
		}}); 
	}
	
	protected void onDestroy() {
        super.onDestroy();
        closeDatabase();
    }
	//開啟資料庫
	private void openDatabase(){
		mHelper = new MyDBHelper(this); 		
	}
	//關閉資料庫
	private void closeDatabase(){
	    mHelper.close();
	}
	//從"Match"table抓取歌單內歌曲id
    public void getMatch(){
    	mDB = mHelper.getWritableDatabase();
        sql = "SELECT * FROM Match WHERE Playlist_id = "+Playlist_id+";";
        cursor = mDB.rawQuery(sql, null);
        cursor.moveToFirst();
        
        _song.clear();
        
        for(int i=0;i<cursor.getCount();i++)
        {
        	_song.add(cursor.getString(1));
        	cursor.moveToNext();
        }
        cursor.close();
        mDB.close();
    }
    //從"Song"table依據歌單內歌曲id抓取歌曲資訊並顯示於listview
    public void getSong(){
    	mDB = mHelper.getWritableDatabase();
    	
    	sql = "SELECT * FROM Song ;";
        cursor = mDB.rawQuery(sql, null);
        
        mDB.execSQL("DROP TABLE IF EXISTS STemp;");
        mDB.execSQL("CREATE TABLE STemp (" +
                "STemp_id INTEGER PRIMARY KEY, " +
                "STemp_name VARCHAR(50) , " +
                "STemp_artist VARCHAR(50) , " +
                "STemp_album VARCHAR(50), " +
                "STemp_size  VARCHAR(50), " +
                "STemp_duration  VARCHAR(50)," +
                "STemp_path  VARCHAR(50));");
        
        title.clear();
        artist.clear();
        path.clear();
        
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        ContentValues values = new ContentValues();
        for(int i=0;i<_song.size();i++)
        {   
        	cursor.moveToFirst();
        	for(int j=0;j<cursor.getCount();j++){
	        	if(_song.get(i).equals(cursor.getString(0))){
	        		HashMap<String, String> map = new HashMap<String, String>();
	                map.put("songTitle", cursor.getString(1));  
	                map.put("songText", cursor.getString(2));
	                map.put("songText2", cursor.getString(5));
	                
	                title.add(cursor.getString(1));
	                artist.add(cursor.getString(2)); 
	                path.add(cursor.getString(6));
	                
	                values.put("STemp_id",cursor.getString(0));
	        		values.put("STemp_name",cursor.getString(1));
	        		values.put("STemp_artist",cursor.getString(2));
	        		values.put("STemp_album",cursor.getString(3));
	        		values.put("STemp_size",cursor.getString(4));
	        		values.put("STemp_duration",cursor.getString(5));
	        		values.put("STemp_path",cursor.getString(6));
	        		mDB.insert("STemp",null,values);	 
	                
	                mylist.add(map);
	                break;
	        	}
	        	cursor.moveToNext();
        	}
        }
        cursor.close();
        mDB.close();
        sql = "SELECT * FROM STemp ;";

        //使用SimpleAdapter建立歌曲列表項目，並且註冊長按功能
        SimpleAdapter mSchedule = new SimpleAdapter(this,mylist,R.layout.songedit_item,new String[] {"songTitle", "songText","songText2"}, new int[] {R.id.songTitle,R.id.songText1,R.id.songText2});
        listView.setAdapter(mSchedule);
        registerForContextMenu(listView);  
    }
    //顯示歌曲資訊
    public void printSongInfo(String sql){
    	mDB = mHelper.getWritableDatabase();
    	cursor = mDB.rawQuery(sql, null);
    	cursor.moveToPosition(currentPosition);
    
    	dispStr = "歌曲名稱："+cursor.getString(1)+ "\n";
    	dispStr += "演出者："+cursor.getString(2)+ "\n";
    	dispStr += "專輯："+cursor.getString(3)+ "\n";
    	dispStr += "大小："+cursor.getString(4)+ "KB\n";
    	dispStr += "長度："+cursor.getString(5)+ "\n";
    	dispStr += "路徑："+cursor.getString(6)+ "\n";
    	
    	cursor.close();
    	mDB.close();
    	new AlertDialog.Builder(ListSongActivity.this).setMessage(dispStr).create().show();
    }
    //搜尋歌單內歌曲
    public void searchSong(){
		key = keyword.getText().toString();
		sql = "SELECT * FROM STemp WHERE STemp_name LIKE '%"+ key+"%';";
		mDB = mHelper.getWritableDatabase();
        cursor = mDB.rawQuery(sql, null);
        cursor.moveToFirst();
        
        _tempSong.clear();
        title.clear();
        artist.clear();
        path.clear();
		
        ArrayList<HashMap<String, String>> searchList = new ArrayList<HashMap<String, String>>();
        for(int i=0;i<cursor.getCount();i++)
        {
            HashMap<String, String> searchMap = new HashMap<String, String>();
            searchMap.put("songTitle", cursor.getString(1)); 
            searchMap.put("songText", cursor.getString(2));
            searchMap.put("songText2", cursor.getString(5));
            
            _tempSong.add(cursor.getString(0));
            title.add(cursor.getString(1));
            artist.add(cursor.getString(2)); 
            path.add(cursor.getString(6));
            
            searchList.add(searchMap);
            cursor.moveToNext();
        }
        cursor.close();
        mDB.close();
        
        SimpleAdapter mSearch = new SimpleAdapter(this,searchList,R.layout.songedit_item,new String[] {"songTitle", "songText","songText2"}, new int[] {R.id.songTitle,R.id.songText1,R.id.songText2});
        listView.setAdapter(mSearch);
	}
    
	protected void onListItemClick(ListView l, View v, int position, long id) {
		 currentPosition = position;
		 Toast.makeText(this, "長按歌曲顯示相關選項", Toast.LENGTH_LONG).show();
	 }
	
	public void onListLongClick(ListView l, View v,int position, long id) {
		 currentPosition = position;
	}
	
	 public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {
			super.onCreateContextMenu(menu, v, menuInfo);

			menu.setHeaderTitle("Menu");
			currentPosition = ((AdapterContextMenuInfo)menuInfo).position;
			
			menu.add(0,dataID,0,"歌曲資訊");
			menu.add(0,deleteID,0,"移除歌曲");
			menu.add(0,exitID,0,"返回");
	}
	 
	 public boolean onContextItemSelected(MenuItem item) {
			super.onContextItemSelected(item);
			switch( item.getItemId() )
			{
			    case dataID:
			    	printSongInfo(sql);
				    break;    			    
			    case deleteID:
			    	AlertDialog.Builder deleteDialog = new AlertDialog.Builder(ListSongActivity.this);
			    	deleteDialog.setTitle("---從歌單中移除歌曲---");
			    	deleteDialog.setMessage("確定要歌單移除歌曲嗎？");
			    	
			    	deleteDialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							sql = "SELECT * FROM Match;";
		    			    mDB = mHelper.getWritableDatabase();
		    			    cursor = mDB.rawQuery(sql, null);
		    			    cursor.moveToPosition(Integer.parseInt(Playlist_id));	
		    			    
							if(key == null){
			    			    mDB.execSQL("DELETE FROM Match WHERE Playlist_id='"+Playlist_id+"' AND Song_id='"+Integer.parseInt(_song.get(currentPosition))+"';");
							}
							else{
								mDB.execSQL("DELETE FROM Match WHERE Playlist_id='"+Playlist_id+"' AND Song_id='"+Integer.parseInt(_tempSong.get(currentPosition))+"';");
								key = "";
								keyword.setText(key);
							}
							
			    			_song.clear();
			    			path.clear();
			    			getMatch();
			    	        getSong(); 
						}	
			    	});
			    	deleteDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {}
			    	});
			    	deleteDialog.show();
					break;			
			    case exitID:
				    break;
			}
			return true;
	}
	public void onItemClick(ListView l, View v, int position, long id) {

	}	
}
